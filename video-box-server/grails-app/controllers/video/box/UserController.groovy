package video.box

import grails.converters.JSON

import org.springframework.http.HttpStatus;

class UserController {
    def userService
    def passwordEncoder

    def createNewUser(User user) {
        userService.createUser(user);
        if (user.hasErrors()) {
            def errors = []
            user.errors.fieldErrors.each {
                // TODO: investigate why if '@' is in args it is encoded into '&#64;'
                errors << message(code: "user.${it.field}.${it.code}", args: [it.rejectedValue]).replace("&#64;", "@")
            }
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
            return render(["errors": errors] as JSON)
        }
        render(["result": "ok"] as JSON)
    }

    def getCurrentUser() {
        User user = userService.getUser(request)
        render([currentUser: user] as JSON)
    }

    def getUserById() {
        def user = User.get(Long.valueOf(params.id))
        render([user: user] as JSON)
    }

    def updateUser() {
        User user = userService.getUser(request)
        user.properties = request.JSON
        user.save()
        render([result: "ok"] as JSON)
    }

    def logout() {
        userService.logout(request)
        render([result: "ok"] as JSON)
    }

    def changePassword() {
        User user = userService.getUser(request)
        if (!passwordEncoder.isPasswordValid(user.password, request.JSON.oldPassword, null)) {
            def errors = []
            errors << "Old password you entered is incorrect."
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
            return render(["errors": errors] as JSON)
        } else {
            user.password = request.JSON.newPassword
            user.save()
        }
        render([result: "ok"] as JSON)
    }
}
