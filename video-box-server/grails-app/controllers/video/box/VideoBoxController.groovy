package video.box

import grails.converters.JSON

import org.apache.commons.io.FilenameUtils

import org.springframework.http.HttpStatus;

class VideoBoxController {

    def imageService
    def videoService
    def userService

    def index() { }

    def getVideoUrls() {
        def videos = videoService.searchVideos(params)
        def urls = []
        videos.each { video ->
            def urlItem = [videoUrl: "${grailsApplication.config.grails.appURL}/videoBox/streamVideo?videoId=${video.id}", posterUrl:"${grailsApplication.config.grails.appURL}/videoBox/showPoster?videoId=${video.id}", id: video.id, shortDesc: video.shortDesc]
            urls << urlItem
        }
        return render([urls: urls] as JSON)
    }

    def getVideoDetails() {
        return render([videoDetails: VideoFile.get(params.videoId)] as JSON)
    }

    def streamVideo() {
        videoService.streamMp4(params, request, response)
    }

    def streamShowcase() {
        videoService.streamShowcase(params, request, response)
    }

    def showPoster() {
        def video = VideoFile.get(params.videoId)
        def posterFile = new File("${video.category.folderPath}${File.separator}${video.posterName}")
        response.setHeader("Content-length", posterFile.bytes.length.toString())
        response.contentType = "image/${imageService.getImageFormat(posterFile.bytes)}"
        response.outputStream << posterFile.bytes
        response.outputStream.flush()
    }

    def showShowcasePoster() {
        def showcase = Showcase.get(params.showcaseId)
        def posterFile = new File("${showcase.posterPath}${File.separator}${showcase.posterName}")
        sendFile(response, posterFile)
    }

    def getShowcase() {
        def showcase = Showcase.list([max: 1])
        return render([showcase: showcase ? showcase.getAt(0): []] as JSON )
    }

    def saveUserFlashBack() {
        User user = userService.getUser(request)
        def flashBack = videoService.saveUserFlashBack(params.videoId, params.playTime, user)
        return render([result: 'ok', flashBack: flashBack] as JSON)
    }

    def getUserFlashBack() {
        User user = userService.getUser(request)
        def flashBack = FlashBack.findByUser(user)
        return render([flashBack: flashBack ?: []] as JSON)
    }

    def deleteUserFlashBack() {
        User user = userService.getUser(request)
        def flashBack = FlashBack.findByUser(user)
        flashBack?.delete()
        return render([result: 'ok'] as JSON)
    }

    def getCategories() {
        return render([categories: Category.list()] as JSON)
    }

    def uploadVideo() {
        def file = request.getFile('videoFile')
        if (file.empty) {
            def errors = []
            errors << 'Video cannot be empty'
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
            return render(["errors": errors] as JSON)
        }

        def poster = request.getFile('posterFile')
        if (poster.empty) {
            def errors = []
            errors << 'Poster cannot be empty'
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
            return render(["errors": errors] as JSON)
        }

        videoService.saveVideo(file, poster, params)
        return render([result: 'ok'] as JSON)
    }

    private void sendFile(response, file) {
        response.setHeader("Content-length", file.bytes.length.toString())
        response.contentType = "image/${imageService.getImageFormat(file.bytes)}"
        response.outputStream << file.bytes
        response.outputStream.flush()
    }
}
