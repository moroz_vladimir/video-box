package video.box

class Category {

    String categoryName;
    String folderPath;

    static constraints = {
    }

    static mapping = {
        version false
        id column: 'category_id'
    }
}
