package video.box

class VideoFile {

    String name;
    String shortDesc;
    String longDesc;
    String starCast;
    String language;
    BigDecimal reviews;
    String awards;
    String posterName;
    String videoFileName
    String searchKeywords;
    Category category;

    static constraints = {
        reviews nullable: true
        shortDesc nullable: true
        longDesc nullable: true
        language nullable: true
        awards nullable: true
        starCast nullable: true
        posterName nullable: true
        searchKeywords nullable: true
    }

    static mapping = {
        version false
        id column: 'video_id'
    }
}
