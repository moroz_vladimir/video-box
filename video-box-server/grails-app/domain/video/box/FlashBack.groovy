package video.box

class FlashBack {

    User user
    VideoFile video
    Long playTime

    static constraints = {

    }

    static mapping = {
        version false
        id column: 'user_id', generator: 'assigned'
        user insertable: false, updateable: false
    }
}
