package video.box

class Role {

    enum RoleName {
        ROLE_USER, ROLE_ADMIN
    }

    String name;

    static constraints = {
        name nullable: false, blank: false, unique: true
    }

    static mapping = {
        cache true
        version false
        id column: 'role_id'
    }

}
