package video.box

class User {

    def transient springSecurityService

    String firstName;
    String lastName;
    String username;
    String password;
    String remarks;
    String status;
    String callback;

    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    Set<Role> getAuthorities() {
        (UserRole.findAllByUser(this) as List<UserRole>)*.role as Set<Role>
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        super.beforeUpdate()
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService?.passwordEncoder ?
                springSecurityService.encodePassword(password) :
                password
    }

    static transients = ['springSecurityService']

    static constraints = {
        username nullable: false, email: true, blank: false, unique: true
        remarks nullable: true
        status nullable: true
        callback nullable: true
    }

    static mapping = {
        version false
        id column: 'user_id'
        password column: '`password`'
    }
}
