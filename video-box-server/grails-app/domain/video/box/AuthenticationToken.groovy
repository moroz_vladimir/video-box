package video.box

class AuthenticationToken {

    String username;
    String token;

    static mapping = {
        version false
        id column: 'auth_id'
    }
}
