package video.box

class Showcase {

    String fileName
    String filePath
    String posterName
    String posterPath

    static constraints = {
    }

    static mapping = {
        version false
        id column: 'showcase_id'
    }
}
