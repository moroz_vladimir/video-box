package video.box

import grails.core.GrailsApplication
import grails.transaction.Transactional
import org.grails.web.util.WebUtils

@Transactional
class UserService {
    static transactional = false

    def springSecurityService
    def tokenGenerator
    def tokenStorageService
    GrailsApplication grailsApplication

    @Transactional
    def createUser(User user) {
        user.save(flush: true);
        if (user.hasErrors()) {
            return
        }
        UserRole.create(user, Role.findByName(Role.RoleName.ROLE_USER))
    }

    def User getUser(def request) {
        AuthenticationToken token = AuthenticationToken.findByToken(request.getHeader(grailsApplication.config.grails.plugin.springsecurity.rest.token.validation.headerName))
        return token ? User.findByUsername(token.username) : null
    }

    def logout(def request) {
        tokenStorageService.removeToken(request.getHeader(grailsApplication.config.grails.plugin.springsecurity.rest.token.validation.headerName))
    }

    def User getCurrentUser() {
        def request = WebUtils.retrieveGrailsWebRequest().currentRequest
        return getUser(request)
    }


}
