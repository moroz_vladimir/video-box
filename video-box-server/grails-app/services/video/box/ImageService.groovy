package video.box

import grails.transaction.Transactional

import javax.imageio.ImageIO
import javax.imageio.ImageReader
import javax.imageio.stream.ImageInputStream

@Transactional
class ImageService {

    String getImageFormat(byte[] bytes) {
        ByteArrayInputStream bis = null
        ImageInputStream iis = null
        try {
            bis = new ByteArrayInputStream(bytes)
            iis = ImageIO.createImageInputStream(bis);
        } finally {
            bis?.close();
        }

        if (!iis) {
            return "jpeg"
        }

        Iterator<ImageReader> imageReaders = ImageIO.getImageReaders(iis);

        return imageReaders.hasNext() ? imageReaders.next().formatName : "jpeg"
    }

}
