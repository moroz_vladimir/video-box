package video.box

import grails.transaction.Transactional

import javax.servlet.ServletOutputStream
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpServletRequest

import org.apache.commons.io.FilenameUtils

@Transactional
class VideoService {

    static int responseBufferSize = 1024*16
    static int transferBufferSize = 1024*16

    static String mimeSeparation = "vb-mime-boundary"

    def grailsApplication

    def searchVideos(Map params) {
        def results = []
        if (params.category) {
            return VideoFile.findAllByCategory(Category.findByCategoryName(params.category))
        }
        if (params.searchText) {
            def criteria = VideoFile.createCriteria()
            results = criteria.list {
                like("searchKeywords", "%${params.searchText.toLowerCase()}%")
            }
        } else {
            results = VideoFile.list()
        }
        return results
    }

    void streamMp4(Map params, HttpServletRequest request, HttpServletResponse response) {

        def video = VideoFile.get(params.videoId)
        def videoFile = new File("${video.category.folderPath}${File.separator}${video.videoFileName}")
        streamFile(videoFile, request, response)
    }

    void streamShowcase(Map params, HttpServletRequest request, HttpServletResponse response) {

        def showcase = Showcase.get(params.showcaseId)
        def showcaseFile = new File("${showcase.filePath}${File.separator}${showcase.fileName}")
        streamFile(showcaseFile, request, response)
    }

    @Transactional
    FlashBack saveUserFlashBack(def videoId, def playTime, def user) {
        def flashBack = FlashBack.findByUser(user)
        if (!flashBack) {
            flashBack = new FlashBack()
            flashBack.user = user
            flashBack.id = user.id
        }
        flashBack.video = VideoFile.get(videoId)
        flashBack.playTime = Long.valueOf(playTime)
        flashBack.save(flush: true)
        return flashBack
    }

    @Transactional
    void saveVideo(def file, def poster, def params) {
        Category category = Category.get(params.category)
        def folder = category.folderPath
        def videoFile = new File(folder, file.originalFilename)
        def posterFile = new File(folder, poster.originalFilename)
        setFileName(file, videoFile)
        setFileName(poster, posterFile)
        file.transferTo(videoFile)
        poster.transferTo(posterFile)
        VideoFile video = new VideoFile()
        video.category = category
        video.name = FilenameUtils.getBaseName(file.originalFilename)
        video.videoFileName = videoFile.name
        video.posterName = posterFile.name
        video.save()
    }

    private void setFileName(srcFile, targetFile) {
        def exists = true;
        def ind = 0;
        while (exists) {
            ind++
            if (!targetFile.exists()) {
                exists = false
            } else {
                def baseName = FilenameUtils.getBaseName(srcFile.originalFilename)
                def extension = FilenameUtils.getExtension(srcFile.originalFilename)
                def newName = "${baseName}_${ind}"
                targetFile = new File(folder, newName)
            }
        }
    }

    private void streamFile(File file, HttpServletRequest request, HttpServletResponse response) {
        // Parse range specifier
        response.setHeader "Cache-Control", "no-store, must-revalidate"
        response.setHeader "Accept-Ranges", "bytes"
        def ranges = parseRange(request, response, file)

        def oStream = response.outputStream

        def contentType = "video/mp4"

        if (!ranges) {
            //Full content response
            response.contentType = contentType
            response.setHeader "Content-Length", file.length().toString()
            oStream << file.newInputStream()
        }
        else {
            // Partial content response.
            response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT)

            if (ranges.size() == 1) {

                Range range = ranges[0]
                response.addHeader "Content-Range", "bytes ${range.start}-${range.end}/$range.length"
                long length = range.end - range.start + 1
                if (length < Integer.MAX_VALUE) {
                    response.setContentLength((int) length)
                }
                else {
                    // Set the content-length as String to be able to use a long
                    response.setHeader "content-length", length.toString()
                }

                response.contentType = contentType

                try {
                    response.setBufferSize(responseBufferSize)
                }
                catch (IllegalStateException e) {
                    log.warn("Can't set HttpServletResponse buffer size.",e)
                }

                if (oStream) {
                    copy(file.newInputStream(), oStream, range)
                }
            }
            else {
                response.setContentType "multipart/byteranges; boundary=$mimeSeparation"

                try {
                    response.setBufferSize(responseBufferSize)
                }
                catch (IllegalStateException e) {
                    log.warn("Can't set HttpServletResponse buffer size.",e)
                }
                if (oStream) {
                    copy(file.newInputStream(), oStream, ranges.iterator(), contentType)
                }
                else {
                    // we should not get here
                    throw new IllegalStateException()
                }
            }
        }
    }

    protected void copy(InputStream istream, ServletOutputStream ostream, Range range) throws IOException {

        IOException exception = copyRange(istream, ostream, range.start, range.end)

        istream.close()

        if (exception) {
            throw exception
        }
    }

    protected void copy(InputStream istream, ServletOutputStream ostream, Iterator<Range> ranges, String contentType) throws IOException {

        IOException exception
        while (!exception && ranges.hasNext()) {

            Range currentRange = ranges.next()

            // Writing MIME header.
            ostream.println()
            ostream.println "--$mimeSeparation"
            if (contentType != null) {
                ostream.println "Content-Type: $contentType"
            }

            ostream.println "Content-Range: bytes ${currentRange.start}-${currentRange.end}/${currentRange.length}"
            ostream.println()

            // Printing content
            exception = copyRange(istream, ostream, currentRange.start, currentRange.end)

            istream.close()
        }

        ostream.println()
        ostream.print "--" + mimeSeparation + "--"

        if (exception) {
            throw exception
        }
    }

    private List<Range> parseRange(HttpServletRequest request, HttpServletResponse response, File videoFile) throws IOException {

        def fileLength = videoFile.length()
        if (fileLength == 0) {
            return null
        }

        def rangeHeader = request.getHeader("Range")
        if (!rangeHeader) {
            return null
        }

        // bytes is the only range unit supported (and I don't see the point of adding new ones)
        if (!rangeHeader.startsWith("bytes")) {
            response.addHeader "Content-Range", "bytes */$fileLength"
            response.sendError HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE
            return null
        }

        rangeHeader = rangeHeader.substring(6)

        // the ranges which are successfully parsed
        def result = []
        def commaTokenizer = new StringTokenizer(rangeHeader, ",")

        // Parsing the range list
        while (commaTokenizer.hasMoreTokens()) {
            def rangeDefinition = commaTokenizer.nextToken().trim()

            def currentRange = new Range()
            currentRange.length = fileLength

            int dashPos = rangeDefinition.indexOf('-')
            if (dashPos == -1) {
                response.addHeader "Content-Range", "bytes */$fileLength"
                response.sendError HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE
                return null
            }

            if (dashPos == 0) {
                try {
                    long offset = Long.parseLong(rangeDefinition)
                    currentRange.start = fileLength + offset
                    currentRange.end = fileLength - 1
                }
                catch (NumberFormatException e) {
                    response.addHeader("Content-Range", "bytes */" + fileLength)
                    response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE)
                    return null
                }
            }
            else {
                try {
                    currentRange.start = Long.parseLong(rangeDefinition.substring(0, dashPos))
                    if (dashPos < rangeDefinition.length() - 1) {
                        currentRange.end = Long.parseLong(rangeDefinition.substring(dashPos + 1, rangeDefinition.length()))
                    }
                    else {
                        currentRange.end = fileLength - 1
                    }
                }
                catch (NumberFormatException e) {
                    response.addHeader "Content-Range", "bytes */$fileLength"
                    response.sendError HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE
                    return null
                }
            }

            if (!currentRange.validate()) {
                response.addHeader("Content-Range", "bytes */" + fileLength)
                response.sendError(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE)
                return null
            }

            result.add(currentRange)
        }

        return result
    }

    private IOException copyRange(InputStream istream, ServletOutputStream ostream, long start, long end) {

        long skipped = 0
        try {
            skipped = istream.skip(start)
        }
        catch (IOException e) {
            return e
        }

        if (skipped < start) {
            return new IOException("start is less than skipped")
        }

        IOException exception
        long bytesToRead = end - start + 1

        byte[] buffer = new byte[transferBufferSize]
        int validBytes = buffer.length
        while ((bytesToRead > 0) && (validBytes >= buffer.length)) {
            try {
                validBytes = istream.read(buffer)
                // if at end of input stream
                if (validBytes<0) {
                    exception = new IOException("Attempt to read past end of input.")
                }
                // if all bytes read should be written
                else if (bytesToRead >= validBytes) {
                    ostream.write(buffer, 0, validBytes)
                    bytesToRead -= validBytes
                }
                // otherwise only write those requested
                else {
                    ostream.write(buffer, 0, (int) bytesToRead)
                    bytesToRead = 0
                }
            }
            catch (IOException e) {
                exception = e
                validBytes = -1
            }
        }

        exception
    }

    private static class Range {

        long start
        long end
        long length

        boolean validate() {
            if (end >= length) {
                end = length - 1
            }
            return (start >= 0) && (end >= 0) && (start <= end) && (length > 0)
        }
    }

}
