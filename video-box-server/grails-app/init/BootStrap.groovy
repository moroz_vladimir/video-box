import grails.converters.JSON

import grails.core.GrailsApplication

import video.box.Category
import video.box.FlashBack
import video.box.RequestMap
import video.box.Role
import video.box.Showcase
import video.box.User
import video.box.VideoFile

class BootStrap {

    GrailsApplication grailsApplication

    def init = { servletContext ->
        populateData()
        registerJSONConverters()
    }

    private void populateData() {
        createRole(Role.RoleName.ROLE_USER.name())
        createRole(Role.RoleName.ROLE_ADMIN.name())

        createRequestMap('/', [Role.RoleName.ROLE_ADMIN.name()])
        createRequestMap('/user/**', [Role.RoleName.ROLE_ADMIN.name(), Role.RoleName.ROLE_USER.name()])
        createRequestMap('/videoBox/**', [Role.RoleName.ROLE_ADMIN.name(), Role.RoleName.ROLE_USER.name()])

        createRequestMap('/login', ['permitAll'])
        createRequestMap('/user/createNewUser', ['permitAll'])
        createRequestMap('/videoBox/showPoster', ['permitAll'])
        createRequestMap('/videoBox/streamVideo', ['permitAll'])
        createRequestMap('/videoBox/getShowcase', ['permitAll'])
        createRequestMap('/videoBox/showShowcasePoster', ['permitAll'])
        createRequestMap('/videoBox/streamShowcase', ['permitAll'])
        createRequestMap('/rest/login', ['permitAll'])
        createRequestMap('/login/**', ['permitAll'])
        createRequestMap('/logout', ['permitAll'])
        createRequestMap('/logout/**', ['permitAll'])

    }

    private registerJSONConverters() {
        JSON.registerObjectMarshaller(Category) {
            def json = [:]
            json.id = it.id
            json.folderPath = it.folderPath
            json.name = it.categoryName
            return json
        }
        JSON.registerObjectMarshaller(VideoFile) {
            def json = [:]
            json.name = it.name
            json.shortDesc = it.shortDesc
            json.longDesc = it.longDesc;
            json.starCast = it.starCast;
            json.language = it.language;
            json.reviews = it.reviews;
            json.awards = it.awards;
            json.posterName = it.posterName;
            json.videoFileName = it.videoFileName;
            json.videoUrl = "${grailsApplication.config.grails.appURL}/videoBox/streamVideo?videoId=${it.id}"
            json.posterUrl = "${grailsApplication.config.grails.appURL}/videoBox/showPoster?videoId=${it.id}"
            return json
        }
        JSON.registerObjectMarshaller(User) {
            def json = [:]
            json.firstName = it.firstName;
            json.lastName = it.lastName;
            json.username = it.username;
            json.remarks = it.remarks;
            json.status = it.status;
            json.callback = it.callback;
            return json
        }
        JSON.registerObjectMarshaller(Showcase) {
            def json = [:]
            json.fileName = it.fileName
            json.filePath = it.filePath
            json.posterName = it.posterName
            json.posterPath = it.posterPath
            json.videoUrl = "${grailsApplication.config.grails.appURL}/videoBox/streamShowcase?showcaseId=${it.id}"
            json.posterUrl = "${grailsApplication.config.grails.appURL}/videoBox/showShowcasePoster?showcaseId=${it.id}"
            return json
        }
        JSON.registerObjectMarshaller(FlashBack) {
            def json = [:]
            json.videoId = it.video.id
            json.playTime = it.playTime
            return json
        }
    }

    private static RequestMap createRequestMap(String url, List configAttributes) {
        RequestMap requestMap = RequestMap.findByUrlAndConfigAttribute(url, configAttributes.join(", "))
        if (!requestMap) {
            requestMap =  new RequestMap(url: url, configAttribute: configAttributes.join(", ")).save(flush: true)
        }
        return requestMap
    }

    private static Role createRole(String roleName) {
        Role role = Role.findByName(roleName)
        if (!role) {
            role = new Role(name: roleName);
            role.save(flush: true)
        }
        return role
    }
}
