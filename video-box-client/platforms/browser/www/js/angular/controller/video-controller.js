'use strict';
videoApp.controller('videoController', ['$scope', 'videoResource',
    function ($scope, videoResource) {

        $scope.duration = 80;
        $scope.searchText = "";

        $scope.initVideoUrls = function () {
            videoResource.getVideoUrls({searchText: $scope.searchText}, function (response) {
                $scope.videoItems = response.urls;
            }, function (response) {
                handleError(response);
            });
        };

        $scope.play = function (playerId) {
            var player = document.getElementById(playerId);
            player.play();
        }

        $scope.pause = function (playerId) {
            var player = document.getElementById(playerId);
            player.pause();
        }

        $scope.stop = function (playerId) {
            var player = document.getElementById(playerId);
            player.stop();
        }

        $scope.preview = function (playerId) {
            var player = document.getElementById(playerId);
            player.currentTime = Math.floor(Math.random() * (player.duration - $scope.duration));
            player.play();
            setTimeout(function(){
                player.pause();
            }, duration)
        } 

    }
]);
