'use strict';

videoApp.requires.push('ngResource');
videoApp.factory('videoResource', ['$resource', 'restApiUrl',
    function ($resource, restApiUrl) {
        return $resource(restApiUrl + '/videoBox/:action', null,
            {
                'getVideoUrls': {
                    method: 'GET',
                    headers: {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'http://ec2-18-222-122-40.us-east-2.compute.amazonaws.com'},
                    params: {
                        action: 'getVideoUrls'
                    }
                }
            });
    }]);