'use strict';

var videoApp = angular.module('videoApp', []);
// TODO made rest api url configurable
videoApp.constant("restApiUrl", "http://ec2-3-16-136-0.us-east-2.compute.amazonaws.com/video-box");
//videoApp.constant("restApiUrl", "http://localhost:8080/video-box");

document.addEventListener("DOMContentLoaded", initDOM, false);


function initDOM() {}

function handleError(response) {
    if (response.status == 500) {
        alert("Something went wrong... We're working on it.");
    }
}

