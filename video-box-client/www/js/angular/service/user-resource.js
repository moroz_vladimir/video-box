'use strict';

videoApp.requires.push('ngResource');
videoApp.factory('userResource', ['$resource', 'restApiUrl',
    function ($resource, restApiUrl) {
        return {
            user: function(token){
                return $resource(restApiUrl + '/user/:action', null,
                    {
                        'createNewUser': {
                            method: 'POST',
                            headers: {'Content-Type': 'application/json'},
                            params: {
                                action: 'createNewUser'
                            }
                        },
                        'getUserById': {
                            method: 'GET',
                            headers: {'Content-Type': 'application/json', 'x-auth-token': token, 'Authorization': 'Bearer ' + token},
                            params: {
                                action: 'getUserById',
                                token: getAccessToken()
                            }
                        },
                        'getCurrentUser': {
                            method: 'GET',
                            headers: {'Content-Type': 'application/json', 'x-auth-token': token, 'Authorization': 'Bearer ' + token},
                            params: {
                                action: 'getCurrentUser',
                                token: getAccessToken()
                            }
                        },
                        'updateUser': {
                            method: 'POST',
                            headers: {'Content-Type': 'application/json', 'x-auth-token': token, 'Authorization': 'Bearer ' + token},
                            params: {
                                action: 'updateUser'
                            }
                        },
                        'logout': {
                            method: 'POST',
                            headers: {'Content-Type': 'application/json', 'x-auth-token': token, 'Authorization': 'Bearer ' + token},
                            params: {
                                action: 'logout'
                            }
                        },
                        'changePassword': {
                            method: 'POST',
                            headers: {'Content-Type': 'application/json', 'x-auth-token': token, 'Authorization': 'Bearer ' + token},
                            params: {
                                action: 'changePassword'
                            }
                        }
            })}};
    }]);