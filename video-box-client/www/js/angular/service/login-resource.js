'use strict';

videoApp.requires.push('ngResource');
videoApp.factory('loginResource', ['$resource', 'restApiUrl',
    function ($resource, restApiUrl) {
        return $resource(restApiUrl + '/rest/:action', null,
            {
                'login': {
                    method: 'POST',
                    headers: {'Content-Type': 'application/json'},
                    params: {
                        action: 'login'
                    }
                }
            });
    }]);