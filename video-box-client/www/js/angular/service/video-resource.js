'use strict';

videoApp.requires.push('ngResource');
videoApp.factory('videoResource', ['$resource', 'restApiUrl',
    function ($resource, restApiUrl) {
        return $resource(restApiUrl + '/videoBox/:action', null,
            {
                'getVideoUrls': {
                    method: 'GET',
                    headers: {'Content-Type': 'application/json', 'x-auth-token': localStorage.getItem("accessToken"), 'Authorization': 'Bearer ' + localStorage.getItem("accessToken")},
                    params: {
                        action: 'getVideoUrls'
                    }
                },
                'getVideoDetails': {
                    method: 'GET',
                    headers: {'Content-Type': 'application/json', 'x-auth-token': localStorage.getItem("accessToken"), 'Authorization': 'Bearer ' + localStorage.getItem("accessToken")},
                    params: {
                        action: 'getVideoDetails'
                    }
                },
                'getShowcase': {
                    method: 'GET',
                    headers: {'Content-Type': 'application/json'},
                    params: {
                        action: 'getShowcase'
                    }
                },
                'saveUserFlashBack': {
                    method: 'GET',
                    headers: {'Content-Type': 'application/json', 'x-auth-token': localStorage.getItem("accessToken"), 'Authorization': 'Bearer ' + localStorage.getItem("accessToken")},
                    params: {
                        action: 'saveUserFlashBack'
                    }
                },
                'getUserFlashBack': {
                    method: 'GET',
                    headers: {'Content-Type': 'application/json', 'x-auth-token': localStorage.getItem("accessToken"), 'Authorization': 'Bearer ' + localStorage.getItem("accessToken")},
                    params: {
                        action: 'getUserFlashBack'
                    }
                },
                'deleteUserFlashBack': {
                    method: 'GET',
                    headers: {'Content-Type': 'application/json', 'x-auth-token': localStorage.getItem("accessToken"), 'Authorization': 'Bearer ' + localStorage.getItem("accessToken")},
                    params: {
                        action: 'deleteUserFlashBack'
                    }
                },
                'getCategories': {
                    method: 'GET',
                    headers: {'Content-Type': 'application/json', 'x-auth-token': localStorage.getItem("accessToken"), 'Authorization': 'Bearer ' + localStorage.getItem("accessToken")},
                    params: {
                        action: 'getCategories'
                    }
                }
            });
    }]);