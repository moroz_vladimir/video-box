'use strict';

videoApp.controller('userController', ['$scope', '$timeout', 'userResource',
    function ($scope, $timeout, userResource) {

        $scope.passwordDetails = {};
        $scope.userDetails = {}
        $scope.confirmPassword = "";

        $scope.createNewUser = function (form, event) {
            $scope.createUserErrors = [];
            form.confirmPassword.$error.match = false;
            event.preventDefault(); // prevents default
            $scope.errorLoginMessage = "";
            if (!form.$valid) {
                return;
            } // if form is invalid
            if ($scope.userDetails.password != $scope.confirmPassword) {
                form.confirmPassword.$error.match = true;
                return;
            }
            userResource.user(null).createNewUser($scope.userDetails, function(response) {
                localStorage.setItem("userName", $scope.userDetails.username);
                localStorage.setItem("password", $scope.userDetails.password);
                alert('You credentials have been saved to database.');
                window.location = 'login.html';
            }, function (response) {
                if (response.data.errors) {
                    $scope.createUserErrors = response.data.errors;
                } else {
                    handleError(response);
                }
            });
        };

        $scope.changePassword = function (form, event) {
            $scope.changePasswordErrors = [];
            form.confirmPassword.$error.match = false;
            event.preventDefault(); // prevents default
            $scope.errorLoginMessage = "";
            if (!form.$valid) {
                return;
            } // if form is invalid
            if ($scope.passwordDetails.newPassword != $scope.passwordDetails.confirmPassword) {
                form.confirmPassword.$error.match = true;
                return;
            }
            userResource.user(getAccessToken()).changePassword($scope.passwordDetails, function(response) {
                window.history.back();
            }, function (response) {
                if (response.data.errors) {
                    $scope.changePasswordErrors = response.data.errors;
                } else {
                    handleError(response);
                }
            });

        };
    }
]);
