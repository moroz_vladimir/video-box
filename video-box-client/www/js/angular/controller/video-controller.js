'use strict';
videoApp.controller('videoController', ['$scope', '$window', '$timeout', 'videoResource', 'restApiUrl',
    function ($scope, $window, $timeout, videoResource, restApiUrl) {

        $scope.duration = 80;
        $scope.searchText = "";

        $scope.initVideoUrls = function () {
            videoResource.getVideoUrls({searchText: $scope.searchText}, function (response) {
                $scope.videoItems = response.urls;
            }, function (response) {
                handleError(response);
            });
            videoResource.getUserFlashBack(function (response) {
                $scope.flashBackItem = response.flashBack;
            }, function (response) {
                handleError(response);
            });
        };

        $scope.searchVideosByCategory = function (category) {
            videoResource.getVideoUrls({category: category}, function (response) {
                $scope.videoItems = response.urls;
            }, function (response) {
                handleError(response);
            });
        };

        $scope.initVideoDetails = function () {
            document.addEventListener("pause", saveFlashbackToStorage, false);
            window.onbeforeunload = function() {
                saveFlashbackToStorage();
            };
            videoResource.getVideoDetails({videoId: getRequestParameter("videoId")}, function (response) {
                $scope.videoDetails = [];
                $scope.videoDetails.push(response.videoDetails);
                $timeout(updateVideoRating, 500);
                if (getRequestParameter("playTime")) {
                    $timeout(initVideoPlayer, 500);
                }
            }, function (response) {
                handleError(response);
            });
        };

        $scope.play = function (playerId) {
            play(playerId, $scope.duration);
        }

        $scope.pause = function (playerId) {
            pause(playerId, $scope.duration);
        }

        $scope.stop = function (playerId) {
            var player = document.getElementById(playerId);
            player.stop();
        }

        $scope.preview = function (playerId) {
            //preview(playerId, $scope.duration);
           videoResource.saveUserFlashBack({videoId: getRequestParameter("videoId"), playTime: Math.round(document.getElementById('myVideo').currentTime)}, function (response) {
                alert('ok');
           }, function (response) {
                        handleError(response);
                    }); 
        }

        $scope.goToVideoList = function () {
            window.location = "index.html";
        }

        $scope.initUpload = function () {
            $scope.uploadVideoUrl = restApiUrl + "/videoBox/uploadVideo";
            window["setUploadVideoUrl"]($scope.uploadVideoUrl);
            videoResource.getCategories(function (response) {
                $scope.categories = response.categories;
            }, function (response) {
                handleError(response);
            });
        }

        $scope.uploadVideo = function () {

        }

        function updateVideoRating() {
            window["updateRating"]($scope.videoDetails[0].reviews);
        }

        function initVideoPlayer() {
            document.getElementById('myVideo').currentTime = getRequestParameter("playTime");
            $scope.play('myVideo');
        }

         function saveFlashbackToStorage() {
             localStorage.setItem("videoId", getRequestParameter("videoId"));
             localStorage.setItem("playTime", document.getElementById('myVideo').currentTime);
             localStorage.setItem("duration", document.getElementById('myVideo').duration);
         }

    }
]);
