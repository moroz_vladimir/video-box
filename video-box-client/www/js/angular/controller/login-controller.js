'use strict';

videoApp.controller('loginController', ['$scope', 'userResource', 'loginResource', 'videoResource',
  function ($scope, userResource, loginResource, videoResource) {
      $scope.userDetails = {};
      $scope.loginDetails = {};
      $scope.errorLoginMessage = '';
      $scope.createUserErrors = [];
      $scope.isLogout = false;
      $scope.login = function (form, event) {
          event.preventDefault(); // prevents default
          $scope.errorLoginMessage = "";
          if (!form.$valid) {
              return;
          } // if form is invalid
          login();
      };

      $scope.createNewUser = function (form, event) {
          $scope.createUserErrors = [];
          form.confirmPassword.$error.match = false;
          event.preventDefault(); // prevents default
          $scope.errorLoginMessage = "";
          if (!form.$valid) {
              return;
          } // if form is invalid
          if ($scope.userDetails.password != $scope.userDetails.confirmPassword) {
              form.confirmPassword.$error.match = true;
              return;
          }
          userResource.user(null).createNewUser($scope.userDetails, function(response) {
                           localStorage.setItem("accessToken", response.access_token);
              localStorage.setItem("userName", $scope.userDetails.username);
              localStorage.setItem("password", $scope.userDetails.password);
          }, function (response) {
              if (response.data.errors) {
                  $scope.createUserErrors = response.data.errors;
              } else {
                  handleError(response);
              }
          });

      };

      $scope.init = function() {
          var isLogout = getRequestParameter("logout");
          if (isLogout) {
              $scope.isLogout = true;
          }
          videoResource.getShowcase(function(response) {
              $scope.showcases = [];
              $scope.showcases.push(response.showcase)
          }, function (response) {
              handleError(response);
          });
      };

      $scope.initApp = function() {
          var userName = localStorage.getItem("userName");
          var password = localStorage.getItem("password");
          if (userName && password) {
              $scope.loginDetails = {"username": userName, "password": password};
              login();
          } else {
              window.location.href = "login.html";
          }
      };

      $scope.logout = function() {
          userResource.user(localStorage.getItem("accessToken")).logout(function(response) {
              localStorage.removeItem("userName");
              localStorage.removeItem("password");
              localStorage.removeItem("accessToken");
              window.location.href = "login.html?logout=true;";
          }, function (response) {
              handleError(response);
          });
      };

      function login() {
          loginResource.login($scope.loginDetails, function(response) {
              localStorage.setItem("accessToken", response.access_token);
              localStorage.setItem("userName", $scope.loginDetails.username);
              localStorage.setItem("password", $scope.loginDetails.password);
              window.location.href = "landing.html";
          }, function (response) {
              $scope.errorLoginMessage = "Username and password you entered do not match.";
          });
      }
  }
]);
