'use strict';

var videoApp = angular.module('videoApp', []);
// TODO made rest api url configurable
videoApp.constant("restApiUrl", "http://ec2-3-16-136-0.us-east-2.compute.amazonaws.com/video-box");
//videoApp.constant("restApiUrl", "http://localhost:8080/video-box");

document.addEventListener("DOMContentLoaded", initDOM, false);

function clearFlashback() {
    localStorage.removeItem("videoId");
    localStorage.removeItem("playTime");
    localStorage.removeItem("duration");
}

function initDOM() {
if (localStorage.getItem("videoId") && localStorage.getItem("playTime")) {
    var injector = angular.element(document.querySelector('[ng-app]')).injector();
    var videoResource = injector.get("videoResource");
    var scope = angular.element(document.querySelector('[ng-app]')).scope();
    if (Math.round(localStorage.getItem("playTime")) < Math.round(localStorage.getItem("duration"))) {
        videoResource.saveUserFlashBack({videoId: localStorage.getItem("videoId"), playTime: Math.round(localStorage.getItem("playTime"))}, function (response) {
            clearFlashback();
            scope.flashBackItem = response.flashBack;
        }, function (response) {
            handleError(response);
       });
    } else {
        videoResource.deleteUserFlashBack(function (response) {
            clearFlashback();
            scope.flashBackItem = null;
        }, function (response) {
            handleError(response);
        });
    }
}}

function handleError(response) {
    if (response.status == 500) {
        alert("Something went wrong... We're working on it.");
    }
}

function getRequestParameter(paramName){
    if(paramName=(new RegExp('[?&]'+encodeURIComponent(paramName)+'=([^&]*)')).exec(location.search)) {
        return decodeURIComponent(paramName[1]);
    } else {
        return null;
    }
}

function getAccessToken() {
    return localStorage.getItem("accessToken");
}

function play(playerId) {
    var player = document.getElementById(playerId);
    player.play();
}

function pause(playerId) {
    var player = document.getElementById(playerId);
    player.pause();
}

function preview(playerId, duration) {
    var player = document.getElementById(playerId);
    player.currentTime = Math.floor(Math.random() * (player.duration - duration));
    player.play();
    setTimeout(function(){
        player.pause();
    }, duration*1000)
}
